// my_math_test.c
#include <stdio.h>
#include <gtest/gtest.h>
#include "my_math.h"

TEST(Math_Test, checkPrimeNumber) {
    EXPECT_EQ(1 , checkPrimeNumber(3));
}

TEST(Math_Test, checkPrimeNumber1) {
    EXPECT_EQ(0 , checkPrimeNumber(8));
    EXPECT_EQ(1 , checkPrimeNumber(7));
}

TEST(Math_Test, checkPrimeNumber2) {
    EXPECT_NE(1 , checkPrimeNumber(8));
    EXPECT_EQ(0 , checkPrimeNumber(4));
}
